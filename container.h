#ifndef CONTAINER_H
#define CONTAINER_H

#include <cstdint>
#include <string>
#include <sstream>

/**
 * @brief The Container class parent of all containers (Array, DEQue...) with some
 * universal methods
 */
class Container
{
public:
	/*
	 * All universally used strings
	 */
	const static std::string STR_ARR;
	const static std::string STR_DEQUE;
	const static std::string STR_BINHEAP;
	const static std::string STR_BST;
	const static std::string STR_RBTREE;

	const static std::string STR_FROM_FILE;
	const static std::string STR_CLEAR;
	const static std::string STR_DELETE;
	const static std::string STR_ADD;
	const static std::string STR_FIND;
	const static std::string STR_RAND;
	const static std::string STR_DISPLAY;
	const static std::string STR_TEST;
	const static std::string STR_BALANCE_TREE;

	const static std::string STR_RET;
	const static std::string STR_QUIT;

	const static std::string STR_INS_CMD;
	const static std::string STR_INS_VAL;
	const static std::string STR_INS_IDX;
	const static std::string STR_INS_N_CNT;
	const static std::string STR_INS_FILE_NAME;
	const static std::string STR_CMD_NOT_FOUND;

	//always assume -1 is equal to last index
	const static int INDEX_LAST = -1;

protected:
	/*
	 * used to fake compiler into thinking that find test calls are doing something
	 * and must not be skipped
	 */
	bool _dontUseThis;

	//lines between menu list and input cursor. Erased every loop pass.
	std::stringstream msg;

	const std::string * MENU_STR[9] =
	{
		&STR_FROM_FILE,
		&STR_RAND,
		&STR_DELETE,
		&STR_ADD,
		&STR_FIND,
		&STR_DISPLAY,
		&STR_TEST,
		&STR_CLEAR,
//		&STR_BALANCE_TREE,
	};

	enum MENU_IDX
	{
		IDX_FROM_FILE = 1,
		IDX_RAND,
		IDX_DELETE,
		IDX_ADD,
		IDX_FIND,
		IDX_DISPLAY,
		IDX_TEST,
		IDX_CLEAR,
		IDX_BALANCE_TREE,
		IDX_RETURN = 9,
		IDX_QUIT = 0,
	};

	//ammount of elements in container
	int length;

	//default value saying how many times test will be repeated
	int testRepeats	= 200;
	//how many elements are tested (added/deleted)
	static constexpr int testCount = 100;
	//ammount of base elements when testing
	static constexpr int testValCnt = 8;
	const int testVals[testValCnt]	= {1000, 2000, 5000, 10000, 15000, 20000, 25000, 30000};
	//width of each test output column
	static constexpr int testStrWidth = 11;
	//precision of fractions in test output
	static constexpr int testStrPrec = 7;
public:
	Container();
	virtual ~Container();

	bool fromFile(std::string filename);
	virtual void fromRandom(int size);

	bool parse();	//returns false only when called STR_RET
	void displayMenu();

	int getCount();

	virtual std::string getName() = 0;
	virtual std::string printValues() = 0;

	virtual void clear() = 0;
	virtual bool add(int32_t val, int index = INDEX_LAST) = 0;
	virtual bool remove(int value);
	virtual bool removeFromIndex(int index);
	virtual bool find(int32_t val) = 0;	//returns -1 when not found, otherwise returns index
	virtual void test() = 0;

};

#endif // CONTAINER_H
