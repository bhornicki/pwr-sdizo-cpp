#include <iostream>
#include <iomanip>
#include <chrono>
#include <random>
#include <sstream>

#include "deque.h"

//double-ended queue element

/**
 * @brief DEQue::getElemAt transform element index to its pointer
 * @param index element at index'th .next jump
 * @return pointer to index'th element or nullptr
 */
DEQue::Element *DEQue::getElemAt(int index)
{
	Element *elem = first;
	while(elem!=nullptr)
	{
		if(!index--)
			return elem;
		elem=elem->next;
	}

	return nullptr;
}

DEQue::DEQue()
{
	first=nullptr;
}

DEQue::~DEQue()
{
	clear();
}

std::string DEQue::getName()
{
	return STR_DEQUE;
}

/**
 * @brief DEQue::printValues prepare string with list traversed forward and then
 * backwards
 * @return string with elements separated by spaces in two lines
 */
std::string DEQue::printValues()
{
	Element *elem = first;
	std::stringstream ss;
	if(elem == nullptr)
	{
		ss << std::endl;
		return ss.str();
	}
	//print all except last
	while(elem->next!=nullptr)
	{
		ss << elem->val << " ";
		elem=elem->next;
	}
	//print last
	ss << elem->val << std::endl;

	//the last will become the first
	while(elem!=nullptr)
	{
		ss << elem->val << " ";
		elem=elem->prev;
	}
	ss << std::endl;

	return ss.str();
}

/**
 * @brief DEQue::clear traverse list and delete each element
 */
void DEQue::clear()
{
	//start from front
	if(first == nullptr)
		return;
	Element *elem = first;

	//jump to next element if exists and delete its previous
	while(elem->next!=nullptr)
	{
		elem=elem->next;
		delete elem->prev;
	}

	//delete last one
	delete elem;

	first = nullptr;
	length = 0;
}

/**
 * @brief DEQue::add inserts value into position
 * @param val value to insert
 * @param index position on which element will be inserted
 * @return true if index is valid and operation succeeded
 */
bool DEQue::add(int32_t val, int index)
{
	//treat -1 as last index
	if(index==INDEX_LAST)
		index = getCount();

	//discard all wrong indexes
	//also when passed index is separated with empty space from last index, it is treated as wrong
	if(index>getCount() || index<0)
		return false;

	//element to be inserted preparation
	Element *ins = new Element;
	ins->val=val;

	//manage adding first element
	if(index==0)
	{
		ins->prev=nullptr;
		ins->next=first;
		first=ins;

		//add back-ref to prev 1st element
		if(ins->next != nullptr)
			ins->next->prev=ins;

		++length;
		return true;
	}

	//go to element just before index
	Element *elem = getElemAt(index-1);
	//it shouldn't ever pass, but just in case I am idiot and did something wrong
	if(elem==nullptr)
		return  false;

	//update inserted element references first
	ins->next=elem->next;
	ins->prev=elem;

	//then update references around insertion to point to ins
	ins->prev->next=ins;
	if(ins->next!=nullptr)
		ins->next->prev=ins;

	++length;
	return true;
}

/**
 * @brief DEQue::removeFromIndex remove element index-wise
 * @param index index of element
 * @return true if index is valid and operation succeeded
 */
bool DEQue::removeFromIndex(int index)
{
	//treat -1 as last index
	if(index==INDEX_LAST)
		index = getCount()-1;

	//discard all wrong indexes
	if(index>=getCount() || index<0)
		return false;

	Element *elem;

	//remove first element
	if(index==0)
	{
		if(first==nullptr)
		{
			//#TODO: debug. delete later
			std::cerr << "Tried to delete DEQue index 0 while first points to null" << std::endl;
			exit(2);
		}

		elem=first;

		first=first->next;
		if(first != nullptr)
			first->prev=nullptr;

		delete elem;
		--length;
		return true;

	}

	//go to element
	elem = getElemAt(index);
	if(elem == nullptr)
		return false;

	//update forward ref
		elem->prev->next = elem->next;
	//don't update backward reference when deleting last element
	if(elem->next != nullptr)
		elem->next->prev=elem->prev;

	delete elem;
	--length;
	return true;
}

/**
 * @brief DEQue::remove  remove first occurence of element value-wise
 * @param value value to delete
 * @return true if value was found and operation succeeded
 */
bool DEQue::remove(int value)
{
	int idx = 0;
	Element *elem = first;
	while(elem != nullptr)
	{
		if(elem->val==value)
			//if it's dumb but it works, it ain't dumb.
			return removeFromIndex(idx);
		elem=elem->next;
		idx++;
	}
	return false;
}

/**
 * @brief DEQue::find find value in a list
 * @param val value to search
 * @return true if found
 */
bool DEQue::find(int32_t val)
{
	Element *elem = first;
	int idx = 0;
	while(elem!=nullptr)
	{
		if(elem->val==val)
			return true;
		elem=elem->next;
		++idx;
	}
	return false;
}

void DEQue::test()
{
	DEQue *arr = new DEQue[testRepeats];
	//prepare random generator
	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);

	std::cout << std::endl << getName() << ", obliczenia dla " << testCount
			  << " elementow, operacja powtarzana " << testRepeats << " razy. " << std::endl
			  << std::endl << std::left
			  << std::setw(7) << "Rozm."
			  << std::setw(testStrWidth) << "Dod. Pocz."
			  << std::setw(testStrWidth) << "Dod. Kon."
			  << std::setw(testStrWidth) << "Dod. Los."
			  << std::setw(testStrWidth) << "Usun Pocz."
			  << std::setw(testStrWidth) << "Usun Kon."
			  << std::setw(testStrWidth) << "Usun Los."
			  << std::setw(testStrWidth) << "Szukaj"
			  << std::setw(testStrWidth) << "Inicjaliz."
			  << std::setw(testStrWidth) << "Razem"
			  << std::endl;

	//tests!
	for (int n = 0; n < testValCnt; n++)
	{
		//init arrays
		auto t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			arr[i].fromRandom(testVals[n]);
		auto t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tinit = t1 - t0;

		//beginning of array  add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(distribution(generator),0);
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].removeFromIndex(0);
		auto t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tba = t1 - t0;
		std::chrono::duration<double> tbr = t2 - t1;

		//end of array add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].removeFromIndex(-1);
		t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tea = t1 - t0;
		std::chrono::duration<double> ter = t2 - t1;

		//random add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
			{
				std::uniform_int_distribution<int> pos(0,arr[i].getCount());
				arr[i].add(distribution(generator), pos(generator));
			}
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
			{
				std::uniform_int_distribution<int> pos(0,arr[i].getCount()-1);
				arr[i].removeFromIndex(pos(generator));
			}
		t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tra = t1 - t0;
		std::chrono::duration<double> trr = t2 - t1;

		//find random value
		t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < testRepeats; ++i)
				for (long int j = 0; j < testCount; ++j)
					_dontUseThis=arr[i].find(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tf = t1 - t0;

		//print everything
		std::chrono::duration<double> tsum  = tba + tbr + tea + ter + tra + trr + tf + tinit;
		std::cout << std::left
				  << std::setw(7) << testVals[n]
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tba.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tea.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tra.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tbr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << ter.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << trr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tf.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tinit.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tsum.count()
					 << std::endl;
	}
	delete [] arr;
}
