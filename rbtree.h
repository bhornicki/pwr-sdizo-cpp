#ifndef RBTREE_H
#define RBTREE_H

#include <sstream>
#include <cstdint>

#include "container.h"

class RBTree : public Container
{
	struct Element
	{
		Element *parent;
		Element *left;
		Element *right;
		int32_t val;
		bool isRed;
	};
	Element *root;

	bool isRight(Element* el);

	bool rotateRight(Element* el);
	bool rotateLeft(Element* el);

	void cleanHelper(Element* base);
	Element* findHelper(Element* base, int32_t val);
	void printHelper(std::stringstream & output, RBTree::Element *base, int indent = 0);

	void remove(Element *el);
	Element *removeHelperFindRep(Element *el);
	void removeHelperDBlack(Element *el);
	int isValidTree(Element *el);
public:
	RBTree();
	~RBTree();

	std::string getName() override;
	std::string printValues() override;

	void clear() override;
	bool add(int32_t val, int index = INDEX_LAST) override;
	bool remove(int value) override;
	bool find(int32_t val) override;
	void test() override;
};

#endif // RBTREE_H
