#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <random>
#include <chrono>
#include <cmath>
#include <algorithm>

#include "rbtree.h"

/**
 * @brief RBTree::isRight tell if element is right child
 * @param el element to check
 * @return true if he's right
 */
bool RBTree::isRight(RBTree::Element *el)
{
	if(el == nullptr || el->parent==nullptr)
		return false;
	return (el->parent->right==el);
}

/**
 * @brief RBTree::rotateRight makes right rotate of branch el
 * @param el element on top with left son
 * @return false if el or el left son is nullptr
 */
bool RBTree::rotateRight(RBTree::Element *el)
{
	if(el == nullptr || el->left == nullptr)
		return false;

	Element *l = el->left;
	Element *p = el->parent;

	//move left to top & update relations between it and parent
	if(p!=nullptr)
	{
		if(isRight(el))
			p->right = l;
		else
			p->left = l;
	}
	else
		root = l;
	l->parent = p;

	//the one between both sons, which gets orphaned and adopted by another element
	el->left=l->right;
	if(l->right!=nullptr)
		l->right->parent=el;

	//move old el to new el right son position and update refs
	el->parent=l;
	l->right=el;
	return true;
}

/**
 * @brief RBTree::rotateLeft makes left rotate of branch el
 * @param el element on top with right son
 * @return false if el or el right son is nullptr
 */
bool RBTree::rotateLeft(RBTree::Element *el)
{
	if(el == nullptr || el->right == nullptr)
		return false;

	Element *r = el->right;
	Element *p = el->parent;

	//move right to top & update relations between it and parent
	if(p!=nullptr)
	{
		if(isRight(el))
			p->right = r;
		else
			p->left = r;
	}
	else
		root = r;
	r->parent = p;

	//the one between both sons, which gets orphaned and adopted by another element
	el->right=r->left;
	if(r->left!=nullptr)
		r->left->parent=el;

	//move old el to new el left son position and update refs
	el->parent=r;
	r->left=el;
	return true;
}

/**
 * @brief RBTree::cleanHelper order childs to kill their childs and then kill themself.
 * Then commit honorary sudoku
 * @param base root of branch which will be killed
 */
void RBTree::cleanHelper(RBTree::Element *base)
{
	if(base==nullptr)
		return;
	cleanHelper(base->left);
	cleanHelper(base->right);
	delete base;
}

/**
 * @brief RBTree::findHelper check branch if value exists
 * @param base root of branch to check
 * @param val value to search for
 * @return pointer to element with value when found, otherwise nullptr
 */
RBTree::Element *RBTree::findHelper(RBTree::Element *base, int32_t val)
{
	if(base==nullptr)
		return nullptr;
	if(base->val==val)
		return base;
	return findHelper((val<base->val)?base->left:base->right, val);
}

/**
 * @brief RBTree::printHelper recursively  print tree pretty (inorder) way
 * @param output ref to stringstream to which tree representation will be pushed
 * @param base root of branch which will be printed
 * @param indent current depth of the tree
 */
void RBTree::printHelper(std::stringstream &output, RBTree::Element *base, int indent)
{
	if (base== nullptr)
		return;

	printHelper(output, base->right,indent+1);

	for (int i = 0; i < indent*3-1;i++)
		output << " ";

	output 	<< (base->parent?(isRight(base)?"/":"\\"):"")
			<< (base->isRed?"R":"B")
			<< base->val<<std::endl;

	printHelper(output, base->left, indent+1);

}

/**
 * @brief RBTree::remove internal function to remove node. Handles various cases to
 * delete.
 * @param el element to delete. Not a nullptr.
 */
void RBTree::remove(RBTree::Element *el)
{
	//find replacement
	Element *rep = removeHelperFindRep(el);
	Element *p = el->parent;

	//el is leaf
	if (rep == nullptr)
	{
		if (el == root)
			root = nullptr;

		else if (!el->isRed)
			removeHelperDBlack(el);

		else if(p != nullptr)
		{
			Element *s = isRight(el)?p->left:p->right;
			if (s != nullptr)
				s->isRed = true;
		}


		if (isRight(el))
			p->right = nullptr;
		else
			p->left = nullptr;
		delete el;
		return;
	}

	// el has 1 child
	if (el->left == nullptr || el->right == nullptr)
	{
		//only two elements in a tree
		if (el == root)
		{
			el->val = rep->val;
			el->left = nullptr;
			el->right = nullptr;
			delete rep;
			return;
		}

		//change el to rep
		if (isRight(el))
			p->right = rep;
		else
			p->left = rep;
		rep->parent = p;

		if ((!rep->isRed) && (!el->isRed))
			removeHelperDBlack(rep);
		else
			//one of 'em was red
			rep->isRed = false;

		delete el;
		return;
	}

	//has 2 children, swap values and repeat
	std::swap(rep->val, el->val);
	remove(rep);
}

/**
 * @brief RBTree::removeHelperFindRep find replacement element
 * @param el base element which needs to be replaced
 * @return element which is suitable to replace el. nullptr when el is leaf
 */
RBTree::Element *RBTree::removeHelperFindRep(RBTree::Element *el)
{
	//is leaf
	if (el->left == nullptr && el->right == nullptr)
		return nullptr;

	//find right successor
	if (el->left != nullptr && el->right != nullptr)
	{
		Element *temp = el->right;

		while (temp->left != nullptr)
			temp = temp->left;

		return temp;
	}

	//when el has one child
	if (el->left != nullptr)
		return el->left;
	else
		return el->right;
}

/**
 * @brief RBTree::removeHelperDBlack handle double-black cases
 * @param el double-black element, non-nullptr assumed
 */
void RBTree::removeHelperDBlack(RBTree::Element *el)
{
	if (el == root)
		return;

	Element *p = el->parent;
	Element *s=nullptr;
	if(p != nullptr)
		s = p->left==el?p->right:p->left;

	if (s == nullptr)
	{
		// No sibiling, double black pushed up
		removeHelperDBlack(p);
		return;
	}
	// Sibling red
	if (s->isRed)
	{
		p->isRed = true;
		s->isRed = false;
		if (isRight(s))
			rotateLeft(p);
		else
			rotateRight(p);

		removeHelperDBlack(el);
		return;
	}

	//sibling and 2 children all black. No children = mentally black.
	//Almost forgotten about 'em.
	if (((s->left!=nullptr && !s->left->isRed)||s->left==nullptr)
		&& ((s->right!=nullptr && !s->right->isRed)||s->right==nullptr))
	{
		s->isRed = true;
		if (!p->isRed)
			removeHelperDBlack(p);
		else
			p->isRed = false;
		return;
	}

	// Sibling black and at least one child red: right one
	if (s->left != nullptr && s->left->isRed)
	{
		if (isRight(s))
		{
			// right left
			s->left->isRed = p->isRed;
			rotateRight(s);
			rotateLeft(p);
		}
		else
		{
			// left left
			s->left->isRed = s->isRed;
			s->isRed = p->isRed;
			rotateRight(p);
		}
		p->isRed = false;
		return;
	}

	// mirror case, left one
	if (s->right != nullptr && s->right->isRed)
	{
		if (!isRight(s))
		{
			// left right
			s->right->isRed = p->isRed;
			rotateLeft(s);
			rotateRight(p);
		}
		else
		{
			// right right
			s->right->isRed = s->isRed;
			s->isRed = p->isRed;
			rotateLeft(p);
		}
		p->isRed = false;
	}
}

/**
 * @brief RBTree::isValidTree checks if tree is valid. Used when add/remove were being
 * implemented.
 * @param el (sub)tree root
 * @return 0 (false) when tree is invalid, otherwise number of black levels.
 */
int RBTree::isValidTree(RBTree::Element *el)
{
	//null child
	if(el==nullptr)
		return 1;

	//two reds in a row
	if(el->right!= nullptr && el->right->isRed && el->isRed)
		return 0;
	if(el->left!= nullptr && el->left->isRed && el->isRed)
		return 0;

	//black count doesn't match
	int r=isValidTree(el->right);
	if(r != isValidTree(el->left))
		return 0;

	//both failed - pass error further
	if(r==0)
		return 0;

	//count itself
	return r + (el->isRed?0:1);
}

RBTree::RBTree()
{
	testRepeats=1000;
	root = nullptr;
}

RBTree::~RBTree()
{
	clear();
}

std::string RBTree::getName()
{
	return STR_RBTREE;
}

/**
 * @brief RBTree::printValues prepare string with tree representation.
 * Rotate Your head counterclockwise 90 deg. to see the tree branches match left and
 * right properly. In other words - upper branch is right branch.
 * Size of indentation informs which layer of tree it is
 * @return string with elements separated by spaces and tree view
 */
std::string RBTree::printValues()
{
	std::stringstream ss;
	printHelper(ss,root);
	return ss.str();
}

/**
 * @brief RBTree::clear recursively delete tree
 */
void RBTree::clear()
{
	length = 0;
	cleanHelper(root);
	root = nullptr;
}

/**
 * @brief RBTree::add adds element as a leaf and corrects tree to match its properties
 * @param val value which will be added
 * @param index ignored
 * @return always true
 */
bool RBTree::add(int32_t val, int index)
{
	//Tushar Roy @Youtube is absolute Indian teachear-god.
	//prepare
	Element * el = new Element;
	el->isRed =	true;
	el->val = val;
	el->right = nullptr;
	el->left = nullptr;
	el->parent = nullptr;
	length++;

	//when adding element first time
	if(root == nullptr)
	{
		root = el;
		el->isRed=false;
		return true;
	}

	//go to adequate leaf
	Element * next = root;
	Element * current;
	while(next != nullptr)
	{
		current = next;
		if(val<current->val)
			next = next->left;
		else
			next = next->right;
	}

	//make leaf adopt a child
	if(val<current->val)
		current->left = el;
	else
		current->right = el;
	el->parent = current;


	/*
	 * RB rebalance
	 */

	Element *p = nullptr;	//parent
	Element *g = nullptr;	//grandpa
	Element *u = nullptr;	//uncle
	bool temp;
	while (el != root && (el->isRed) && (el->parent->isRed))
	{
		p = el->parent;
		g = p->parent;
		u = ((g->right==p)?g->left:g->right);

		//two red childs = red parent
		if (u!=nullptr && u->isRed)
		{
			u->isRed=false;
			p->isRed=false;
			g->isRed=true;
			el = g;
			continue;
		}

		if(isRight(p))
		{
			if (!isRight(el))
			{
				//RL
				rotateRight(p);
				el = p;
				p = el->parent;
			}
			rotateLeft(g);
		}
		else
		{
			if (isRight(el))
			{
				//LR
				rotateLeft(p);
				el = p;
				p = el->parent;
			}
			rotateRight(g);
		}

		//swap colors
		temp = p->isRed;
		p->isRed = g->isRed;
		g->isRed = temp;
		el = p;

	}

	//Root is red! red! Fix it booyah!
	root->isRed=false;
	return true;
}

/**
 * @brief RBTree::remove remove element from tree and fix colors
 * @param value value which will be deleted
 * @return true if succeeded
 */
bool RBTree::remove(int value)
{
	Element *el = findHelper(root,value);
	if(el==nullptr)
		return false;
	remove(el);
	length--;
	return true;
}

/**
 * @brief RBTree::find call recursive search for val
 * @param val value which will be searched
 * @return true if val exists
 */
bool RBTree::find(int32_t val)
{
	return findHelper(root,val)!=nullptr;
}

void RBTree::test()
{
	RBTree *arr = new RBTree[testRepeats];

	//prepare random generator
	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);

	std::cout << std::endl << getName() << ", obliczenia dla " << testCount
			  << " elementow, operacja powtarzana " << testRepeats << " razy. " << std::endl
			  << std::endl << std::left
			  << std::setw(7) << "Rozm."
			  << std::setw(testStrWidth) << "Dodawanie"
			  << std::setw(testStrWidth) << "Usuwanie"
			  << std::setw(testStrWidth) << "Szukaj"
			  << std::setw(testStrWidth) << "Inicjaliz."
			  << std::setw(testStrWidth) << "Razem"
			  << std::endl;

	//tests!
	for (int n = 0; n < testValCnt; n++)
	{
		//init arrays
		auto t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			arr[i].fromRandom(testVals[n]);

		//create LUT
		int32_t LUT[testRepeats][testCount];
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				LUT[i][j]=distribution(generator);
		auto t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tinit = t1 - t0;

		//add
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(LUT[i][j]);
		t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> ta = t1 - t0;


		//remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].remove(LUT[i][j]);
		t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tr = t1 - t0;

		//find random value
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (long int j = 0; j < testCount; ++j)
				_dontUseThis=arr[i].find(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tf = t1 - t0;

		//print everything
		std::chrono::duration<double> tsum  = ta + tr + tf + tinit;
		std::cout << std::left
				  << std::setw(7) << testVals[n]
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << ta.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tf.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tinit.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tsum.count()
					 << std::endl;
	}
	delete [] arr;
}

