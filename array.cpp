#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <random>
#include <chrono>
#include <algorithm>

#include "array.h"

Array::Array()
{
	testRepeats = 1000;
	array=nullptr;
}

Array::~Array()
{
	clear();
}

/**
 * @brief Array::fromRandom optimised from random to add elements in one batch
 * @param size ammount of elements in a container after call of this function
 */
void Array::fromRandom(int size)
{
	array = (int32_t*)realloc(array,sizeof(int32_t)*size);
	length=size;

	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);

	for (int i = 0; i < size; ++i) {
		array[i]=distribution(generator);
	}

}

std::string Array::getName()
{
	return STR_ARR;
}

/**
 * @brief Array::printValues prepare string with values
 * @return string with elements
 */
std::string Array::printValues()
{
	std::stringstream ss;

	for (int i = 0; i < length; ++i)
		ss << array[i] << " ";

	ss << std::endl;
	return  ss.str();
}

/**
 * @brief DEQue::clear delete array
 */
void Array::clear()
{
	free(array);
	array = nullptr;
	length = 0;
}

/**
 * @brief Array::add inserts value into position
 * @param val value to insert
 * @param index position on which element will be inserted
 * @return true if index is valid and operation succeeded
 */
bool Array::add(int32_t val, int index)
{
	//treat -1 as last index
	if(index==INDEX_LAST)
		index = getCount();

	//discard all wrong indexes
	//also when passed index is separated with empty space from last index, it is treated as wrong
	if(index>getCount() || index<0)
		return false;

	array=(int32_t*)realloc(array,sizeof(int32_t)*length+1);
	for (int i = length; i > index; --i)
		array[i]=array[i-1];
	array[index]=val;
	length++;
	return true;
}

/**
 * @brief Array::removeFromIndex remove element index-wise
 * @param index index of element
 * @return true if index is valid and operation succeeded
 */
bool Array::removeFromIndex(int index)
{
	//treat -1 as last index
	if(index==INDEX_LAST)
		index = getCount()-1;

	//discard all wrong indexes
	if(index>=getCount() || index<0)
		return false;

	//when we have only one element
	if(length==1)
	{
		free(array);
		array=nullptr;
		length = 0;
		return true;
	}

	for (int i = index+1; i <length; i++)
		array[i-1]=array[i];
	array=(int32_t*)realloc(array,sizeof(int32_t)*length--);
	return true;
}

/**
 * @brief Array::find find value in array
 * @param val value to search
 * @return true if found
 */
bool Array::find(int32_t val)
{
	return std::find(array,array+length,val) != array+length;
}

void Array::test()
{
	Array *arr = new Array[testRepeats];

	//prepare random generator
	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);

	std::cout << std::endl << getName() << ", obliczenia dla " << testCount
			  << " elementow, operacja powtarzana " << testRepeats << " razy. " << std::endl
			  << std::endl << std::left
			  << std::setw(7) << "Rozm."
			  << std::setw(testStrWidth) << "Dod. Pocz."
			  << std::setw(testStrWidth) << "Dod. Kon."
			  << std::setw(testStrWidth) << "Dod. Los."
			  << std::setw(testStrWidth) << "Usun Pocz."
			  << std::setw(testStrWidth) << "Usun Kon."
			  << std::setw(testStrWidth) << "Usun Los."
			  << std::setw(testStrWidth) << "Szukaj"
			  << std::setw(testStrWidth) << "Inicjaliz."
			  << std::setw(testStrWidth) << "Razem"
			  << std::endl;

	//tests!
	for (int n = 0; n < testValCnt; n++)
	{
		//init arrays
		auto t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			arr[i].fromRandom(testVals[n]);
		auto t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tinit = t1 - t0;

		//beginning of array  add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(distribution(generator),0);
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].removeFromIndex(0);
		auto t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tba = t1 - t0;
		std::chrono::duration<double> tbr = t2 - t1;

		//end of array add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].removeFromIndex(-1);
		t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tea = t1 - t0;
		std::chrono::duration<double> ter = t2 - t1;

		//random add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
			{
				std::uniform_int_distribution<int> pos(0,arr[i].getCount());
				arr[i].add(distribution(generator), pos(generator));
			}
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
			{
				std::uniform_int_distribution<int> pos(0,arr[i].getCount()-1);
				arr[i].removeFromIndex(pos(generator));
			}
		t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tra = t1 - t0;
		std::chrono::duration<double> trr = t2 - t1;

		//find random value
		t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < testRepeats; ++i)
				for (long int j = 0; j < testCount; ++j)
					_dontUseThis = arr[i].find(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tf = t1 - t0;

		//print everything
		std::chrono::duration<double> tsum  = tba + tbr + tea + ter + tra + trr + tf + tinit;
		std::cout << std::left
				  << std::setw(7) << testVals[n]
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tba.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tea.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tra.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tbr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << ter.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << trr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tf.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tinit.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tsum.count()
					 << std::endl;
	}
	delete [] arr;
}
