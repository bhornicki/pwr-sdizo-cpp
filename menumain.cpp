#include "menumain.h"
#include <iostream>

#include "array.h"
#include "deque.h"
#include "binheap.h"
#include "rbtree.h"

MenuMain::MenuMain()
{
	container = nullptr;
}

/**
 * @brief MenuMain::displayMenu display container selection menu. Does not parse input.
 */
void MenuMain::displayMenu()
{
	std::cout << std::endl << std::endl << std::endl;
	for (int i = 1; i < 5; ++i)
		std::cout << i << ". " << *MENU_STR[i-1] << std::endl;

	std::cout << "0. " << Container::STR_QUIT << std::endl;
}

/**
 * @brief MenuMain::parse when container is selected, passes execution to it,
 * otherwise displays menu and parses user input.
 * Call only this function instead of display
 */
void MenuMain::parse()
{

	/*
	 * container sub-menu
	 */

	if(container!=nullptr)
	{
		container->displayMenu();
		if(container->parse())
			//consume keypress event
			return;
		else
		{
			//destroy submenu when called STR_RET
			delete container;
			container = nullptr;
		}
	}

	/*
	 * main menu
	 */
	displayMenu();

	//get user input
	int i;
	std::cout << Container::STR_INS_CMD;
	std::cin >> i;

	switch (i)
	{
		case IDX_ARR:
			container = new Array();
			break;
		case IDX_DEQUE:
			container = new DEQue();
			break;
		case IDX_BINHEAP:
			container = new BinHeap();
			break;
		case IDX_RBTREE:
			container = new RBTree();
			break;
		case IDX_QUIT:
			exit(0);
		default:
			std::cout << "Nie rozpoznano polecenia" << std::endl;
			break;
	}
}

