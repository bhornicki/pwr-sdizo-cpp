#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <random>
#include <chrono>
#include <cmath>
#include <algorithm>

#include "binheap.h"

int BinHeap::getParentIdx(int i)
{
	if(i<=0)
		return -1;
	return (i-1)/2;
}

int BinHeap::getLeftIdx(int i)
{
	return (2*i + 1);
}

int BinHeap::getRightIdx(int i)
{
	return (2*i + 2);
}

bool BinHeap::isValidIdx(int i)
{
	return !(i>=length||i<0);

}

void BinHeap::swap(int idx1, int idx2)
{
	int32_t temp = array[idx1];
	array[idx1]=array[idx2];
	array[idx2] = temp;
}

BinHeap::BinHeap(int overhead)
{
	if(overhead<0)
	{
		std::cerr << "Zapas miejsca nie moze byc ujemny!" << std::endl;
		exit(3);
	}
	array=(int32_t*)malloc(sizeof(int32_t)*overhead);
	this->overhead=overhead;
	lengthAllocation=overhead;
	testRepeats=1000;
}

BinHeap::~BinHeap()
{
	free(array);
}

std::string BinHeap::getName()
{
	return STR_BINHEAP;
}

/**
 * @brief BinHeap::printValues prepare string with heap as array and heap as tree.
 * Rotate Your head counterclockwise 90 deg. to see the tree branches match left and
 * right properly. In other words - upper branch is right branch.
 * Size of indentation informs which layer of tree it is
 * @return string with elements separated by spaces and tree view
 */
std::string BinHeap::printValues()
{
	std::stringstream ss;
	if(!length)
		return "";

	for (int i = 0; i < length; ++i)
		ss << array[i] << " ";
	ss << std::endl << std::endl;
	printHelper(ss,0);

	return ss.str();
	/*
	 * This code is kept here because I'm sentimental
	//lazy way
	for (int idx = 0; idx < length; ++idx)
	{
		ss<<array[idx]<<" ";
		if(!(idx%2))
			ss<<"  ";
		//check if idx is equal to power of 2 xD
		//this is stupid. But it works!
		if(std::pow(2,(int)log2(idx+2))==idx+2)
			ss<<std::endl;
	}
	if(log2(length+1)!=(int)log2(length+1))
		ss<<std::endl;
	return ss.str();
	*/
}

/**
 * @brief BinHeap::printHelper recursively display tree pretty way
 * @param output stringstream to output to
 * @param index current element
 */
void BinHeap::printHelper(std::stringstream & output, int index)
{
	//This code was stolen :)
	int child = 2 * index + 2;
	if (child < length)
		printHelper(output, child);
	int element_level = (int)log2(index);

	if (index == (int)exp2(element_level + 1) - 1)
		element_level++;

	for (int i = 0; i < element_level*3;i++)
	{
		if (i == element_level*3 - 1)
		{
			if (index % 2 == 0)
				output << "/";
			else
				output << "\\";
		}else
			output << " ";
	}

	output << array[index]<<std::endl;


	child = 2 * index + 1;
	if (child < length)
		printHelper(output, child);

}

/**
 * @brief BinHeap::clear delete array
 */
void BinHeap::clear()
{
	array = (int32_t*)realloc(array,sizeof(int32_t)*overhead);
	length = 0;
	lengthAllocation=overhead;
}

/**
 * @brief BinHeap::assureOverhead makes sure that at least overhead count of values
 * will fit in container without need for reallocating.
 * @param overhead number of elements which can be inserted without reallocating
 */
void BinHeap::assureOverhead(int overhead)
{
	if(overhead<0)
	{
		std::cerr << "Zapas miejsca nie moze byc ujemny!" << std::endl;
		exit(3);
	}

	if(lengthAllocation-length>=overhead)
		return;
	lengthAllocation=length+overhead;
	array = (int32_t*)realloc(array,sizeof(int32_t)*lengthAllocation);
	this->overhead=overhead;


}

/**
 * @brief BinHeap::add adds element as a leaf and corrects its position in binary heap
 * to match its properties
 * @param val value which will be added
 * @param index ignored
 * @return always true
 */
bool BinHeap::add(int32_t val, int index)
{
	//check and provide empty space
	if(length>=lengthAllocation)
	{
		if(length>lengthAllocation)
		{
			//should not ever happen, but just in case.
			std::cerr << "Ilosc przechowywanych danych przekroczyla rozmiar kontenera!" << std::endl;
			exit(4);
		}
		lengthAllocation=length+overhead;
		array = (int32_t*)realloc(array,sizeof(int32_t)*lengthAllocation);
	}

	//add
	index = length++;
	array[index]=val;

	//fix binheap properties
	int parent;
	while(isValidIdx(parent=getParentIdx(index)))
	{
		if(array[index]>array[parent])
		{
			swap(index,parent);
			index=parent;
		}
		else
			break;
	}
	return true;
}

/**
 * @brief BinHeap::remove deletes value from tree
 * @param value value to delete
 * @return true when succeeded
 */
bool BinHeap::remove(int value)
{
	//find node
	int idx=-1;
	for (int i = 0; i< length; ++i)
		if(array[i]==value)
		{
			idx=i;
			break;
		}
	if(idx==-1)
		return false;

	//replace with last val
	array[idx]=array[--length];

	//all done when deleted last element
	if(!isValidIdx(idx))
		return true;

	//compare to parent
	int next;
	while(isValidIdx(next=getParentIdx(idx)))
		if(array[idx] == array[next])
			return true;
		else if(array[idx] < array[next])
			break;
		else
		{
			swap(idx,next);
			idx=next;
		}

	//compare to bigger child
	while(isValidIdx(next=getLeftIdx(idx)))
	{
		//if right exist, replace next when its val is greater than left
		if(isValidIdx(getRightIdx(idx))&&(array[next]<array[getRightIdx(idx)]))
			next = getRightIdx(idx);

		if(array[next]>array[idx])
			swap(idx,next);
		else
			break;
		idx=next;
	}

	return true;
}

/**
 * @brief BinHeap::find search array-style for element
 * @param val value to search for
 * @return true if element found
 */
bool BinHeap::find(int32_t val)
{
	return std::find(array,array+length,val) != array+length;
}

/**
 * @brief BinHeap::find2 my attempt to recursively optimise searching - if element value
 * is lower than searched, there is no purpose to check check rest of branch (max
 * binheap always has lower value son), check current element value if matches, call
 * find2 check on left son and if still not found check right branch.
 * @param val value to search for
 * @param idx which element (and its sons) will be checked now
 * @return true if value found
 */
bool BinHeap::find2(int32_t val, int idx)
{
	if(!isValidIdx(idx) || array[idx]<val)
		return false;
	if(array[idx]==val)
		return true;
	if(find2(val,getLeftIdx(idx)))
		return  true;
	return find2(val,getRightIdx(idx));

}

void BinHeap::test()
{
	BinHeap *arr = new BinHeap[testRepeats];

	//prepare random generator
	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);

	std::cout << std::endl << getName() << ", obliczenia dla " << testCount
			  << " elementow, operacja powtarzana " << testRepeats << " razy. " << std::endl
			  << std::endl << std::left
			  << std::setw(7) << "Rozm."
			  << std::setw(testStrWidth) << "Dodawanie"
			  << std::setw(testStrWidth) << "Usuwanie"
			  << std::setw(testStrWidth) << "Szukaj"
			  << std::setw(testStrWidth) << "Szukaj v2"
			  << std::setw(testStrWidth) << "Inicjaliz."
			  << std::setw(testStrWidth) << "Razem"
			  << std::endl;

	//tests!
	for (int n = 0; n < testValCnt; n++)
	{
		//init arrays
		auto t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
		{
			arr[i].fromRandom(testVals[n]);
			arr[i].assureOverhead(testCount + 10);
		}

		//create LUT
		int32_t LUT[testRepeats][testCount];
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				LUT[i][j]=distribution(generator);
		auto t1 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tinit = t1 - t0;

		//add & remove
		t0 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
				arr[i].add(LUT[i][j]);
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (int j = 0; j < testCount; ++j)
			{
				arr[i].remove(LUT[i][j]);
			}
		auto t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> ta = t1 - t0;
		std::chrono::duration<double> tr = t2 - t1;

		//find random value
		t0 = std::chrono::high_resolution_clock::now();
			for (int i = 0; i < testRepeats; ++i)
				for (long int j = 0; j < testCount; ++j)
					_dontUseThis=arr[i].find(distribution(generator));
		t1 = std::chrono::high_resolution_clock::now();
		for (int i = 0; i < testRepeats; ++i)
			for (long int j = 0; j < testCount; ++j)
				_dontUseThis=arr[i].find2(distribution(generator));
		t2 = std::chrono::high_resolution_clock::now();
		std::chrono::duration<double> tf = t1 - t0;
		std::chrono::duration<double> tf2 = t2 - t1;

		//print everything
		std::chrono::duration<double> tsum  = ta + tr + tf +tf2+ tinit;
		std::cout << std::left
				  << std::setw(7) << testVals[n]
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << ta.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tr.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tf.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tf2.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tinit.count()
					 << std::setw(testStrWidth) << std::setprecision(testStrPrec) << tsum.count()
					 << std::endl;
	}
	delete [] arr;
}
