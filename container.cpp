#include "container.h"
#include <random>
#include <cinttypes>
#include <iostream>
#include <fstream>
#include <chrono>

const std::string Container::STR_ARR		= "Tabela";
const std::string Container::STR_DEQUE		= "Lista dwukierunkowa";
const std::string Container::STR_BINHEAP	= "Kopiec binarny";
const std::string Container::STR_BST		= "Drzewo przeszukiwan binarnych BST";
const std::string Container::STR_RBTREE		= "Drzewo czerwono-czarne";

const std::string Container::STR_FROM_FILE		= "Z pliku";
const std::string Container::STR_DELETE			= "Usun";
const std::string Container::STR_CLEAR			= "Wyczysc kontener";
const std::string Container::STR_ADD			= "Dodaj";
const std::string Container::STR_FIND			= "Znajdz";
const std::string Container::STR_RAND			= "Utworz losowo";
const std::string Container::STR_DISPLAY		= "Wyswietl";
const std::string Container::STR_TEST			= "Test";
const std::string Container::STR_BALANCE_TREE	= "Równowazenie drzewa";

const std::string Container::STR_RET	= "Powrot";
const std::string Container::STR_QUIT	= "Koniec";

const std::string Container::STR_INS_CMD		= "Wybierz polecenie: ";
const std::string Container::STR_INS_VAL		= "Podaj wartosc: ";
const std::string Container::STR_INS_IDX		= "Podaj indeks: ";
const std::string Container::STR_INS_FILE_NAME	= "Podaj nazwe pliku: ";
const std::string Container::STR_CMD_NOT_FOUND	= "Nie rozpoznano polecenia";
const std::string Container::STR_INS_N_CNT		= "Podaj ilosc elementow: ";

Container::Container()
{
	length = 0;
}

Container::~Container()
{

}

/**
 * @brief Container::fromFile Make array from file. Universal for all containers, because
 * it's not worth being optimised.
 * First line contains element (line) count in file, which will be added.
 * As professor said, program mustn't be idiotproof, so proper element count in first
 * line is assumed.
 * @param filename file to open
 * @return
 */
bool Container::fromFile(std::string filename)
{
	std::ifstream is;
	is.open(filename);
	if(!is.is_open())
		return false;
	clear();

	int cnt;
	int32_t val;

	is>>cnt;
	while(cnt--)
	{
		is>>val;
		add(val);
	}

	is.close();
	return true;
}

/**
 * @brief Container::fromRandom default random size container generator.
 * @param size final size of container after this call
 */
void Container::fromRandom(int size)
{
	clear();
	std::default_random_engine generator(time(0));
	std::uniform_int_distribution<int32_t> distribution(INT32_MIN,INT32_MAX);
	while(size--)
		add(distribution(generator),0);

}

int Container::getCount()
{
	return length;
}

/**
 * @brief Container::parse parse user input.
 * Proper input - only numbers is assumed.
 * @return true if menu is still valid, false when user wants to go back
 */
bool Container::parse()
{
	int i;
	std::cout << STR_INS_CMD;
	std::cin >> i;

	switch (i)
	{
		case IDX_ADD:
		{
			int idx = INDEX_LAST;
			int32_t val;
			if(getName()==STR_ARR||getName()==STR_DEQUE)
			{
				std::cout << STR_INS_IDX;
				std::cin>>idx;
			}
			std::cout << STR_INS_VAL;
			std::cin>>val;
			if(!add(val,idx))
				msg << "Operacja nie powiodla sie" << std::endl;
			//			printValues();
			break;
		}
		case IDX_DELETE:
		{
			int idx;
			std::cout << ((getName()==STR_ARR)?STR_INS_IDX:STR_INS_VAL);
			std::cin>>idx;
			if(getName()==STR_ARR)
			{
				if(!removeFromIndex(idx))
					msg << "Operacja nie powiodla sie" << std::endl;
			}
			else if(!remove(idx))
				msg << "Operacja nie powiodla sie" << std::endl;
			break;
		}
		case IDX_FIND:
		{
			int32_t val;
			std::cout << STR_INS_VAL;
			std::cin>>val;
			if(find(val))
				msg << "Znaleziono wartosc" << std::endl;
			else
				msg << "Wartosc nie znajduje sie w kontenerze" << std::endl;
			break;
		}
		case IDX_DISPLAY:
			msg << printValues();
			break;
		case IDX_FROM_FILE:
		{
			std::string file;
			std::cout << STR_INS_FILE_NAME;
			std::cin>>file;
			if(!fromFile(file))
				msg << "Operacja nie powiodla sie" << std::endl;
			break;
		}
		case IDX_RAND:
		{
			int32_t cnt;
			std::cout << STR_INS_N_CNT;
			std::cin>>cnt;
			fromRandom(cnt);
			break;
		}
		case IDX_TEST:
			test();
			break;
		case IDX_CLEAR:
			clear();
			break;
		case IDX_RETURN:
			return false;
		case IDX_QUIT:
			exit(0);
//		case IDX_BALANCE_TREE:
//			if(getName()!=STR_BST)
//			{
//				msg << STR_CMD_NOT_FOUND << std::endl;
//				break;
//			}
//			((BST *) this)->balance();

		default:
			msg << STR_CMD_NOT_FOUND << std::endl;
			break;
	}

	return true;
}

/**
 * @brief Container::displayMenu insert a few empty lines, display container content
 * then empty line, menu options, previous command output and user input
 */
void Container::displayMenu()
{
	std::cout << std::endl << std::endl << std::endl
			  << printValues() << std::endl
			  << getName() << std::endl;

	for (int i = 1; i <= IDX_BALANCE_TREE; ++i)
	{
		//don't print balance tree option when container != BST
		if(i==IDX_BALANCE_TREE && getName()!=STR_BST)
			continue;

		std::cout << i << ". " << *MENU_STR[i-1] << std::endl;
	}

	std::cout << "9. " << STR_RET << std::endl
			  << "0. " << STR_QUIT << std::endl;

	if(msg.str().length()>0)
	{
		std::cout << msg.str();
		msg.str("");
	}


}

/**
 * @brief Container::remove Throw error when not reimplemented.
 * Normally remove first occurence of element value-wise.
 * @param value value to delete
 * @return normally true if value was found and operation succeeded
 */
bool Container::remove(int value)
{
	std::cerr<< "Blad! Usuwanie po wartosci nie zaimplementowane!";
	exit(-2);
}

/**
 * @brief Container::removeFromIndex Throw error when not reimplemented.
 * Normally remove first occurence of element index-wise.
 * @param value value to delete
 * @return normally true if value was found and operation succeeded
 */
bool Container::removeFromIndex(int index)
{
	std::cerr<< "Blad! Usuwanie po indeksie nie zaimplementowane!";
	exit(-2);
}
