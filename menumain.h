#ifndef MENUMAIN_H
#define MENUMAIN_H

#include <string>
#include "container.h"

class MenuMain
{
	const std::string * MENU_STR[6] =
	{
		&Container::STR_ARR,
		&Container::STR_DEQUE,
		&Container::STR_BINHEAP,
//		&Container::STR_BST,
		&Container::STR_RBTREE,
	};

	enum MENU_IDX
	{
		IDX_ARR = 1,
		IDX_DEQUE,
		IDX_BINHEAP,
//		IDX_BST,
		IDX_RBTREE,
		IDX_QUIT = 0,
	};

	Container *container;
public:
	MenuMain();
	void displayMenu();
	void parse();
};

#endif // MENUMAIN_H
