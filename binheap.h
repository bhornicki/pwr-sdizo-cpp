#ifndef BINHEAP_H
#define BINHEAP_H
#include <sstream>
#include "container.h"

class BinHeap: public Container
{
	int32_t *array;
	//size of entire array
	int lengthAllocation;
	//how much additional space will be allocated during reallocation
	int overhead;
private:
	int getParentIdx(int i);
	int getLeftIdx(int i);
	int getRightIdx(int i);
	bool isValidIdx(int i);
	void swap(int idx1, int idx2);

	void printHelper(std::stringstream & output, int index);
public:
	BinHeap(int overhead = 100);
	~BinHeap();

	std::string getName() override;
	std::string printValues() override;

	void assureOverhead(int overhead);
	void clear() override;
	bool add(int32_t val, int index = INDEX_LAST) override;
	bool remove(int value) override;
	bool find(int32_t val) override;
	bool find2(int32_t val, int idx=0);
	void test() override;
};

#endif // BINHEAP_H
