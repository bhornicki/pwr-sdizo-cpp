#include <iostream>

#include "menumain.h"
#include <iostream>
using namespace std;

int main()
{
	MenuMain mm;

	std::cout << "SDiZO Projekt 1" << std::endl
			  << "Autor: Bartosz Hornicki" << std::endl
			  << "Prowadzacy: Dr inz. Jaroslaw Mierzwa" << std::endl
			  << std::endl
			  << "Wszystkie indeksy liczone sa od 0." << std::endl
			  << "Do ostatniego elementu mozna odwolac sie rowniez poprzez indeks -1."
			  << "W przypadku tablicy i listy element zostanie dodany gdy jego indeks jest nie wiekszy niz liczba elementow" << std::endl
			  << std::endl;

	while (1)
	{
		mm.parse();
	}
}
