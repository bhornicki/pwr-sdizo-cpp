#ifndef DEQUE_H
#define DEQUE_H

//double-ended queue
#include <cinttypes>
#include "container.h"



class DEQue: public Container
{
	struct Element
	{
		Element *prev;
		Element *next;
		int32_t val;
	};

	Element *first;
	Element *getElemAt(int index);
public:
	DEQue();
	~DEQue();

	std::string getName() override;
	std::string printValues() override;

	void clear() override;
	bool add(int32_t val, int index = INDEX_LAST) override;
	bool removeFromIndex(int index) override;
	bool remove(int value) override;
	bool find(int32_t val) override;
	void test() override;

};

#endif // DEQUE_H
