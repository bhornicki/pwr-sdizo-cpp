#ifndef ARRAY_H
#define ARRAY_H
#include <cinttypes>
#include "container.h"


class Array: public Container
{
	int32_t *array;
public:
	Array();
	~Array();

	void fromRandom(int size) override;

	std::string getName() override;
	std::string printValues() override;

	void clear() override;
	bool add(int32_t val, int index = INDEX_LAST) override;
	bool removeFromIndex(int index) override;
	bool find(int32_t val) override;
	void test() override;
};

#endif // ARRAY_H
